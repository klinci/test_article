<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ArticleController extends Controller
{
    /**
     * @Route("/article", name="article")
     */
    public function index(Request $request)
    {
        $message = '';
        if(!is_null($request->get('message'))) $message = $request->get('message');

        $articles = $this->getDoctrine()
                        ->getRepository(Article::class)
                        ->findAll();

        return $this->render('article/index.html.twig', [
              'articles' => $articles,
              'message' => $message
        ]);
    }

    /**
     * @Route("/article/show/{id}", name="show_article")
     */
    public function show($id)
    {
        $article = $this->getDoctrine()
                        ->getRepository(Article::class)
                        ->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'No article found for id '.$id
            );
        }

        return $this->render('article/show.html.twig', ['article' => $article]);
    }

    /**
     * @Route("/article/add", name="add_article")
     */
    public function add(Request $request)
    {
        $message = '';
        if(!is_null($request->get('message'))) $message = $request->get('message');

        return $this->render('article/add.html.twig', [
            'message' => $message
        ]);
    }

    /**
     * @Route("/article/store", name="store_article")
     */
    public function store(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $article = new Article();
        $article->setName($request->get('name'));
        $article->setContent($request->get('content'));

        $entityManager->persist($article);
        $entityManager->flush();

        return $this->redirectToRoute('add_article', [
            'message' => 'Article ' . $article->getName() . ' added successfully.'
        ]);
    }

    /**
     * @Route("/article/edit/{id}", name="edit_article")
     */
     public function edit($id, Request $request)
     {
         $message = '';
         if(!is_null($request->get('message'))) $message = $request->get('message');

         $article = $this->getDoctrine()
                         ->getRepository(Article::class)
                         ->find($id);

         if (!$article) {
             throw $this->createNotFoundException(
                 'No article found for id '.$id
             );
         }

         return $this->render('article/edit.html.twig', [
             'message' => $message, 'article' => $article
         ]);
     }

     /**
      * @Route("/article/update/{id}", name="update_article")
      */
      public function update($id, Request $request)
      {
          $entityManager = $this->getDoctrine()->getManager();

          $article = $this->getDoctrine()
                          ->getRepository(Article::class)
                          ->find($id);

          $article->setName($request->get('name'));
          $article->setContent($request->get('content'));

          $entityManager->persist($article);
          $entityManager->flush();

          return $this->redirectToRoute('edit_article', [
              'message' => 'Article ' . $article->getName() . ' updated successfully.',
              'id' => $id
          ]);
      }

      /**
       * @Route("/article/delete/{id}", name="delete_article")
       */
       public function delete($id)
       {
           $entityManager = $this->getDoctrine()->getManager();

           $article = $this->getDoctrine()
                           ->getRepository(Article::class)
                           ->find($id);

           $articleName = $article->getName();

           $entityManager->remove($article);
           $entityManager->flush();

           return $this->redirectToRoute('article', [
               'message' => 'Article ' . $articleName . ' deleted successfully.'
           ]);
       }
}

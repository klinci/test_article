<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ArticleController extends Controller
{
    /**
     * @Route("/article", name="article")
     */
    public function index(Request $request)
    {
        $message = '';
        if(!is_null($request->get('message'))) $message = $request->get('message');

        $articles = $this->getDoctrine()
                        ->getRepository(Article::class)
                        ->findAll();

        return $this->render('article/index.html.twig', [
              'articles' => $articles,
              'message' => $message
        ]);
    }

    /**
     * @Route("/article/show/{slug}", name="show_article")
     */
    public function show($slug)
    {
        $article = $this->getDoctrine()
                        ->getRepository(Article::class)
                        ->findOneBy(['slug' => $slug]);

        if (!$article) {
            throw $this->createNotFoundException(
                'No article found for id '. $article->getId()
            );
        }

        return $this->render('article/show.html.twig', ['article' => $article]);
    }

    /**
     * @Route("/article/add", name="add_article")
     */
    public function add(Request $request)
    {
        $message = '';
        if(!is_null($request->get('message'))) $message = $request->get('message');

        return $this->render('article/add.html.twig', [
            'message' => $message
        ]);
    }

    /**
     * @Route("/article/store", name="store_article")
     */
    public function store(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $article = new Article();
        $article->setName($request->get('name'));
        $article->setContent($request->get('content'));
        $article->setSlug($request->get('name'));

        $entityManager->persist($article);
        $entityManager->flush();

        return $this->redirectToRoute('add_article', [
            'message' => 'Article ' . $article->getName() . ' added successfully.'
        ]);
    }

    /**
     * @Route("/article/edit/{slug}", name="edit_article")
     */
     public function edit($slug, Request $request)
     {
         $message = '';
         if(!is_null($request->get('message'))) $message = $request->get('message');

         $article = $this->getDoctrine()
                         ->getRepository(Article::class)
                         ->findOneBy(['slug' => $slug]);

         if (!$article) {
             throw $this->createNotFoundException(
                 'No article found for id '. $article->getId()
             );
         }

         return $this->render('article/edit.html.twig', [
             'message' => $message, 'article' => $article
         ]);
     }

     /**
      * @Route("/article/update/{slug}", name="update_article")
      */
      public function update($slug, Request $request)
      {
          $entityManager = $this->getDoctrine()->getManager();

          $article = $this->getDoctrine()
                          ->getRepository(Article::class)
                          ->findOneBy(['slug' => $slug]);

          $article->setName($request->get('name'));
          $article->setContent($request->get('content'));;
          $article->setSlug($request->get('name'));

          $entityManager->persist($article);
          $entityManager->flush();

          return $this->redirectToRoute('edit_article', [
              'message' => 'Article ' . $article->getName() . ' updated successfully.',
              'slug' => $article->getSlug()
          ]);
      }

      /**
       * @Route("/article/delete/{slug}", name="delete_article")
       */
       public function delete($slug)
       {
           $entityManager = $this->getDoctrine()->getManager();

           $article = $this->getDoctrine()
                           ->getRepository(Article::class)
                           ->findOneBy(['slug' => $slug]);

           $articleName = $article->getName();

           $entityManager->remove($article);
           $entityManager->flush();

           return $this->redirectToRoute('article', [
               'message' => 'Article ' . $articleName . ' deleted successfully.'
           ]);
       }
}
